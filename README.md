# Camelot Detector

Service for facilitating image detection. Uses [MegaDetector](https://github.com/microsoft/CameraTraps), part of [AI for Earth](https://www.microsoft.com/en-us/ai/ai-for-earth), under the hood.

## Development

Camelot Detector relies heavily on cloud resources, particularly those
provided by [Azure](https://azure.microsoft.com/).

### Configuration

The cloud resources described above are also used including while running locally, and so much be configured in your development environment.

The details of these are configured using environment variables. The following environment variables must be set appropriately:

* `SENDGRID_API_KEY`: API key for sending emails via [SendGrid](https://sendgrid.com/).
* `AZURE_SUBSCRIPTION_ID`: Azure Subscription ID.
* `AZURE_TENANT_ID`: Azure Tenant ID.
* `CAMELOT_DETECTOR_AZURE_CLIENT_ID`: Azure Client ID.
* `CAMELOT_DETECTOR_AZURE_CLIENT_SECRET`: Azure Client Secret.
* `CAMELOT_DETECTOR_MONGODB_URI`: MongoDB connection URI.
* `CAMELOT_DETECTOR_MONGODB_DATABASE_NAME`: MongoDB database name to use.
* `CAMELOT_DETECTOR_MONGODB_ACCOUNTS_COLLECTION`: MongoDB collection name for accounts.
* `CAMELOT_DETECTOR_EMAIL_FROM_ADDRESS`: Email address which messages are sent from.
* `CAMELOT_DETECTOR_STORAGE_RESOURCE_GROUP_NAME`: Name of the azure resource group to use for storage.
* `CAMELOT_DETECTOR_STORAGE_PREFIX`: Prefix for any storage accounts to be created.
* `CAMELOT_DETECTOR_APPINSIGHTS_KEY`: Application Insights key for use with logging.
* `CAMELOT_DETECTOR_APPINSIGHTS_ENV`: Environment name for application insights logging (i.e., `dev`, `stg` or `prod`).
* `CAMELOT_DETECTOR_MEGADETECTOR_API`: URL of the batch detector service.

### Running

1. Install [virtualenv](https://virtualenv.pypa.io/en/latest/): `pip install virtualenv`.
2. Run `script/run`.
3. Open http://localhost:8080 in your web browser.

### Usage

Check out the [API documentation](https://api.stg.camelotproject.org/ui/).

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
