import pytz
from camelot_detector.shared.http_utils import make_get_request, make_post_request
from camelot_detector.shared.system import get_component
from enum import Enum
import re
from datetime import datetime

STATUS_KEY = 'Status'
TIMESTAMP_KEY = 'Timestamp'
NOT_FOUND_STATUS = 'Not found.'

def _task_to_payload(caller, task):
    return {
        "input_container_sas": task['container']['readonly_sas'],
        "model_version": "4.1",
        "caller": caller,
        "request_name": str(task['_id'])
    }

def _task_status(resp):
    if STATUS_KEY in resp:
        return resp[STATUS_KEY]['request_status']
    if 'status' in resp and "request_status" in resp['status']:
        return resp['status']['request_status']
    else:
        return None

def get_api_version(api_base):
    if '/v2/' in api_base:
        return 'v2'
    return 'v4'

def submit_request(api_base, caller, task):
    version = get_api_version(api_base)
    try:
        payload = _task_to_payload(caller, task)
        resp = make_post_request(
            "%s/%s" % (api_base, 'request_detections'),
            data=payload)
        get_component('log').event({
            'state': 'ok',
            'service': 'detector task client submit api_' + version + ' success count',
            'description': 'Client submit successful',
            'metric': 1,
            'tags': ['task', 'detector', 'client', 'submit', 'success', version, 'count']
        })
        return resp
    except Exception as e:
        get_component('log').event({
            'state': 'warn',
            'service': 'detector task client submit api_' + version + ' failed count',
            'description': 'Client submit failed',
            'metric': 1,
            'tags': ['task', 'detector', 'client', 'submit', 'failed', version, 'count']
        })
        raise e

def parse_timestamp(t):
    if ' ' in t:
        # V2 API format
        tz_time = t + ' +0000'
        return datetime.strptime(tz_time, '%Y-%m-%d %H:%M:%S %z')
    else:
        # V4 API format
        tz_time = t.replace("Z", "")
        d = datetime.strptime(tz_time, '%Y-%m-%dT%H:%M:%S.%f')
        return d.astimezone(pytz.utc)

FILENAME_PATTERN = re.compile(".*_detections_([^_]+?)_.*\.json.*")
def task_id_from_filename(search):
    m = re.match(FILENAME_PATTERN, search)
    return m.group(1)

def record_metric(version, status):
    status_lc = status.lower()
    if status in ['PROBLEM', 'FAILED']:
        state = 'warn'
    else:
        state = 'ok'
    get_component('log').event({
        'state': state,
        'service': 'detector task client processing api_' + version + ' status ' + status_lc + ' count',
        'description': 'Check ' + status_lc,
        'metric': 1,
        'tags': ['task', 'detector', 'client', 'processing', 'check', version, status_lc, 'count']
    })

def check_processing_v4(api_base, request_id):
    # TODO handle not found status 404'ing
    resp = make_get_request("%s/%s/%s" % (api_base, 'task', request_id))
    status = _task_status(resp)
    if status == 'completed':
        if isinstance(resp[STATUS_KEY]['message'], str):
            record_metric(get_api_version(api_base), 'FAILED_ERROR')
            return ('FAILED', {'response_data': {"error": resp[STATUS_KEY]['message']}})
        else:
            output_urls = resp[STATUS_KEY]['message']['output_file_urls']
            data = {
                "task_id": task_id_from_filename(output_urls['detections']),
                "start_time": parse_timestamp(resp[TIMESTAMP_KEY]),
                "end_time": datetime.now(pytz.utc),
                'detections': output_urls.get('detections'),
                'images': output_urls.get('images'),
                'failed_images': output_urls.get('failed_images')
            }
            record_metric(get_api_version(api_base), 'COMPLETED')
            return ('COMPLETED', data)
    elif status == 'problem':
        data = {
            'response_data': {"error": resp[STATUS_KEY]['message']},
            "start_time": parse_timestamp(resp[TIMESTAMP_KEY]),
            "end_time": datetime.now(pytz.utc)
        }
        record_metric(get_api_version(api_base), 'PROBLEM')
        return ('PROBLEM', data)
    elif status == 'failed':
        data = {
            'response_data': {"error": resp[STATUS_KEY]['message']},
            "start_time": parse_timestamp(resp[TIMESTAMP_KEY]),
            "end_time": datetime.now(pytz.utc)
        }
        record_metric(get_api_version(api_base), 'FAILED')
        return ('FAILED', data)
    elif STATUS_KEY in resp and resp[STATUS_KEY] == NOT_FOUND_STATUS:
        data = {
            'response_data': {"error": "Job Lost"},
            'job_lost': True
        }
        record_metric(get_api_version(api_base), 'FAILED_JOB_LOST')
        return ('FAILED', data)
    elif status == 'running':
        record_metric(get_api_version(api_base), 'RUNNING')
        return ('SUBMITTED', resp)
    else:
        record_metric(get_api_version(api_base), 'UNKNOWN')
        return ('SUBMITTED', resp)

def check_processing_v2(api_base, request_id):
    # TODO remove this for v4 adoption
    # New format request_id; cannot be processed.
    if '-' in request_id:
        data = {
            'response_data': {"error": "Invalid job"},
            'invalid_job': True
        }
        return ('FAILED', data)

    resp = make_get_request("%s/%s/%s" % (api_base, 'task', request_id))
    status = _task_status(resp)

    if status == 'completed':
        if isinstance(resp['status']['message'], str):
            record_metric(get_api_version(api_base), 'FAILED_ERROR')
            return ('FAILED', {'response_data': {"error": resp['status']['message']}})
        else:
            output_urls = resp['status']['message']['output_file_urls']
            data = {
                "task_id": task_id_from_filename(output_urls['detections']),
                "start_time": parse_timestamp(resp['timestamp']),
                "end_time": parse_timestamp(resp['status']['time']),
                'detections': output_urls['detections'],
                'images': output_urls['images'],
                'failed_images': output_urls['failed_images']
            }
            record_metric(get_api_version(api_base), 'COMPLETED')
            return ('COMPLETED', data)
    elif status == 'problem':
        data = {
            'response_data': {"error": resp['status']['message']},
            "start_time": parse_timestamp(resp['timestamp']),
            "end_time": parse_timestamp(resp['status']['time'])
        }
        record_metric(get_api_version(api_base), 'PROBLEM')
        return ('PROBLEM', data)
    elif status == 'failed':
        data = {
            'response_data': {"error": resp['status']['message']},
            "start_time": parse_timestamp(resp['timestamp']),
            "end_time": parse_timestamp(resp['status']['time'])
        }
        record_metric(get_api_version(api_base), 'FAILED')
        return ('FAILED', data)
    elif 'status' in resp and resp['status'] == 'n':
        data = {
            'response_data': {"error": "Job Lost"},
            'job_lost': True
        }
        record_metric(get_api_version(api_base), 'FAILED_JOB_LOST')
        return ('FAILED', data)
    elif status == 'running':
        record_metric(get_api_version(api_base), 'RUNNING')
        return ('SUBMITTED', resp)
    else:
        record_metric(get_api_version(api_base), 'UNKNOWN')
        return ('SUBMITTED', resp)

def check_processing(api_base, request_id):
    if get_api_version(api_base) == 'v2':
        return check_processing_v2(api_base, request_id)
    return check_processing_v4(api_base, request_id)
