"""Task model utilities."""

import secrets
import uuid
import camelot_detector.shared.constants as constants
from datetime import datetime, timedelta

def generate_container_name():
    """Return a new apikey."""
    return secrets.token_hex(16)

def calculate_expiry(dt=None):
    if not dt:
        dt = datetime.utcnow()
    expiry_dt = dt + constants.CONTAINER_EXPIRY_TIMEDELTA
    return int(expiry_dt.timestamp() * 1000)

def is_expired(expiry_dt, dt=None):
    if not dt:
        dt = datetime.utcnow()
    return expiry_dt < int(dt.timestamp() * 1000)

def generate_task(account, container):
    """Create a new account."""
    return {
        'account_id': account['_id'],
        'container': container,
        'container_expiry': calculate_expiry(),
        'status': 'PENDING'
    }
