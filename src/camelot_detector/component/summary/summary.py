"""Process state summary"""

from .model import generate_account, generate_apikey
from bson.objectid import ObjectId

DETECTOR_STATE_KEY='detector'

class Summary:
    """Application state summary."""

    def __init__(self, Logger, Database, SendMail):
        self.log = Logger
        self.summary = Database.get_summary()
        self.sendmail = SendMail

    def increment_in_flight_tasks(self):
        """Register a new user account for `email_address`.
        Returns `None` if the user account already exists."""
        if self.summary.find_one_and_update(
                {'_id': ObjectId()},
                {'$inc': {'in_flight_tasks': 1}},
            return_document=ReturnDocument.AFTER
        )
        existing_account = self._get_account_by_email(email_address)
        if existing_account:
            self.log.warn("Account", "Attempt to re-register an email address owned by user %s" % existing_account['_id'])
            self.log.event({
                'state': 'warning',
                'service': 'detector account registration already exists count',
                'metric': 1,
                'tags': ['account', 'registration', 'already_exists', 'count']
            })
            return None

        account = generate_account(email_address)
        self._create_account(account)
        return account

    def finalise_registration(self, account, sa_name):
        """Associate the storage account and notify the user of registration."""
        self._associate_storage_account(account['_id'], sa_name)
        self._send_registration_email(account)

    def authenticate(self, email_address, apikey):
        """Returns the account, or None if it cannot be validated."""
        account = self._get_account_by_email(email_address)
        if account and account['apikey'] == apikey:
            self.log.event({
                'state': 'ok',
                'service': 'detector account authentication succeeded count',
                'metric': 1,
                'tags': ['account', 'authentication', 'authenticated', 'count']
            })
            return to_account(account)
        else:
            self.log.event({
                'state': 'warning',
                'service': 'detector account authentication failed count',
                'metric': 1,
                'tags': ['account', 'authentication', 'incorrect_api_key', 'count']
            })
            return None

    def get_account(self, account_id):
        """Retrieve account data for the account with the given ID.
        Returns None if it cannot be found."""
        account = self.accounts.find_one({'_id': account_id})
        if not account:
            return None
        else:
            return to_account(account)

    def _send_registration_email(self, account):
        self.sendmail.send(
            account['email_address'],
            'Camelot Project registration',
            """Welcome to the Camelot Project.
            <p>
            Your API key is: %s
            </p>""" % account['apikey']
        )
        self.log.event({
            'state': 'ok',
            'service': 'detector account registration email sent count',
            'metric': 1,
            'tags': ['account', 'registration', 'email_sent', 'count']
        })

    def _associate_storage_account(self, account_id, sa_name):
        """Associate the account with the storage account."""
        self.accounts.find_one_and_update(
            {'_id': account_id},
            {'$set': {'storage_account_name': sa_name}}
        )
        self.log.event({
            'state': 'ok',
            'service': 'detector storage account created count',
            'metric': 1,
            'tags': ['account', 'storageaccount' 'associated', 'count']
        })

    def _get_account_by_email(self, email_address):
        """Return true if an account for the given email already exists.
        False otherwise."""
        return self.accounts.find_one({'email_address': email_address})

    def _create_account(self, account):
        """Insert the given account into the collection."""
        account = self.accounts.insert_one(account)
        self.log.event({
            'state': 'ok',
            'service': 'detector account registration created count',
            'metric': 1,
            'tags': ['account', 'registration', 'created', 'count']
        })
        return account
