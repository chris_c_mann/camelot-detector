"""Message processor."""

from bson.objectid import ObjectId
import traceback

class MessageProcessor:
    """Process messages."""

    def __init__(self, Logger, Task, Account):
        self.log = Logger
        self.task = Task
        self.account = Account

    def _handle_task(self, message):
        task_id = message['_id']
        account_id = ObjectId(message['account_id'])
        account = self.account.get_account(account_id)

        if not account:
            self.log.event({
                'state': 'ok',
                'service': 'detector message_processor account notfound count',
                'metric': 1,
                'tags': ['message_processor', 'account', 'not_found', 'count']
            })
            self.log.warn("MessageProcessor", "Could not find account: %s" % account_id)
            return

        task = self.task.get(account, task_id, skip_fetch=True)
        if task and self.task.is_expired(task):
            self.task.archive(account, task_id)
            self.log.event({
                'state': 'ok',
                'service': 'detector message_processor task archived count',
                'metric': 1,
                'tags': ['message_processor', 'task', 'archived', 'count']
            })
        else:
            self.log.event({
                'state': 'ok',
                'service': 'detector message_processor task checked count',
                'metric': 1,
                'tags': ['message_processor', 'task', 'checked', 'count']
            })

    def handle(self, message):
        try:
            self._handle_task(message)
        except Exception as e:
            self.log.event({
                'state': 'ok',
                'service': 'detector message_processor task processingerror count',
                'metric': 1,
                'tags': ['message_processor', 'task', 'processing_error', 'count']
            })
            self.log.error("MessageProcessor", "Failed to process task %s: %s\n%s" % (message.get('_id'), str(e), traceback.print_exc()))
            task_id = message['_id']
            account_id = ObjectId(message['account_id'])
            account = self.account.get_account(account_id)
            task = self.task.get_task_nocheck(account, task_id)
            if task and self.task.is_expired(task):
                self.log.event({
                    'state': 'ok',
                    'service': 'detector message_processor task archived count',
                    'metric': 1,
                    'tags': ['message_processor', 'task', 'archived', 'count']
                })
                self.task.archive(account, task_id)
