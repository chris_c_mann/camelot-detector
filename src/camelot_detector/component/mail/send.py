"""Send emails"""

import os
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from datetime import datetime, timedelta

def default_configuration():
    return {
        'from_address': os.getenv('CAMELOT_DETECTOR_EMAIL_FROM_ADDRESS'),
        'apikey': os.getenv('SENDGRID_API_KEY')
    }

class SendMail:
    """Send email."""

    def __init__(self, Logger, configuration=None, Client=SendGridAPIClient):
        if not configuration:
            configuration = default_configuration()
        self.log = Logger
        self.client = Client(configuration['apikey'])
        self.from_address = configuration['from_address']
        self.mail = Mail

    def send(self, recipient, subject, html_content):
        """Sends an email"""
        start = datetime.now().timestamp() * 1000
        message = Mail(
            from_email=self.from_address,
            to_emails=recipient,
            subject=subject,
            html_content=html_content)
        try:
            self.client.send(message)
            latency = round((datetime.now().timestamp() * 1000) - start)
            self.log.event({
                'state': 'ok',
                'service': 'detector account email dispatch latency',
                'metric': latency,
                'tags': ['email', 'email_dispatch', 'latency']
            })
            self.log.info("SendMail", "Email sent to %s" % recipient)
        except Exception as e:
            latency = round((datetime.now().timestamp() * 1000) - start)
            self.log.event({
                'state': 'critical',
                'service': 'detector account email dispatch latency',
                'metric': latency,
                'tags': ['email', 'email_dispatch', 'latency', 'failed']
            })
            self.log.error("SendMail", "Failed to send email to %s: %s" % (recipient, str(e)))
