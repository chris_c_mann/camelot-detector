"""Account model utilities."""

import secrets

def generate_apikey():
    """Return a new apikey."""
    return secrets.token_urlsafe()

def generate_account(email_address, storage_account):
    """Create a new account."""
    return {
        'email_address': email_address,
        'apikey': generate_apikey(),
        'storage_account_name': storage_account
    }
