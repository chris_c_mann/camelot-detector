"""Logging."""

import os
import socket
import logging
import bernhard

class Logger:
    def __init__(self, app):
        self.env = os.getenv('CAMELOT_DETECTOR_APPINSIGHTS_ENV')
        self.riemann_server = os.getenv('CAMELOT_DETECTOR_RIEMANN_SERVER')
        if app:
            self.logger = app.logger
        else:
            self.logger = logging
        self.bernhard = bernhard.Client(host=self.riemann_server, port=5555)
        self.base_metric_data = {
            'host': socket.gethostname(),
            'service': 'detector'
        }

    def error(self, context, message):
        self.logger.error('@%s: %s: %s', self.env, context, message)

    def warn(self, context, message):
        self.logger.warn('@%s: %s: %s', self.env, context, message)

    def info(self, context, message):
        self.logger.info('@%s: %s: %s', self.env, context, message)

    def event(self, metric):
        if self.riemann_server:
            combined = { **self.base_metric_data, **metric }
            try:
                self.bernhard.send(combined)
            except Exception as e:
                self.error('logger', 'Failed to send metrics: ' + str(e))
        else:
            self.warn('logger', metric)

