"""Connect to a MongoDB database."""

import os
from pymongo import MongoClient

def default_configuration():
    return {
        'client': os.getenv('CAMELOT_DETECTOR_MONGODB_URI'),
        'db': os.getenv('CAMELOT_DETECTOR_MONGODB_DATABASE_NAME')
    }

class Database:
    """Manage MongoDB."""

    def __init__(self, Logger, database_configuration=None, Client=MongoClient):
        if not database_configuration:
            database_configuration = default_configuration()
        self.client = Client(database_configuration['client'])
        self.db = self.client(database_configuration['db'])

    def getAccounts(self):
        return self.db[os.getenv('CAMELOT_DETECTOR_ACCOUNTS_COLLECTION')]
