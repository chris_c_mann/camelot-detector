"""Handle clean up task queue messages."""

import os
import json
import threading
from azure.storage.queue import (QueueClient,TextBase64EncodePolicy,TextBase64DecodePolicy)

class CleanUpScheduler:
    """Clean up scheduler."""

    def __init__(self, Logger, Task, TaskQueue):
        self.task = Task
        self.log = Logger
        self.task_queue = TaskQueue

    def go(self):
        thread = threading.Thread(target=self._task, daemon=True)
        thread.start()
        self.log.event({
            'state': 'ok',
            'service': 'detector clean_up initiated count',
            'metric': 1,
            'tags': ['clean_up', 'initiated', 'count']
        })

    def _task(self):
        tasks = self.task.get_all_non_archived()
        for task in tasks:
            self.log.event({
                'state': 'ok',
                'service': 'detector clean_up task count',
                'metric': 1,
                'tags': ['clean_up', 'task', 'count']
            })

            # Seeing strange bug where scheduled clean up fails
            if '_id' not in task:
                self.log.warn("CleanUp", "Task without ID: %s" % str(task))
                continue

            message = {
                '_id': str(task['_id']),
                'account_id': str(task['account_id'])
            }
            self.task_queue.produce(message)
