"""Azure storage account management"""

from os import environ
from msrestazure.azure_exceptions import CloudError
from azure.common.credentials import ServicePrincipalCredentials
from azure.mgmt.storage import StorageManagementClient
from datetime import datetime, timedelta
from azure.mgmt.storage.models import (
    StorageAccountCreateParameters,
    Sku,
    SkuName,
    Kind
)

def default_configuration():
    return {
        "common_storage_account": environ['CAMELOT_DETECTOR_COMMON_STORAGE_ACCOUNT'],
        "storage_account_prefix": environ['CAMELOT_DETECTOR_STORAGE_PREFIX'],
        "resource_group_name": environ['CAMELOT_DETECTOR_STORAGE_RESOURCE_GROUP_NAME'],
        "subscription_id": environ['AZURE_SUBSCRIPTION_ID'],
        "credentials": ServicePrincipalCredentials(
            tenant=environ['AZURE_TENANT_ID'],
            client_id=environ['CAMELOT_DETECTOR_AZURE_CLIENT_ID'],
            secret=environ['CAMELOT_DETECTOR_AZURE_CLIENT_SECRET']
        )
    }

STORAGE_CREATE_PARAMETERS = StorageAccountCreateParameters(
    sku=Sku(name=SkuName.standard_ragrs),
    kind=Kind.storage,
    location='southcentralus'
)

class Storage:
    """Manage storage accounts."""

    def __init__(self,
                 Logger,
                 storage_configuration=None,
                 Client=StorageManagementClient):
        if not storage_configuration:
            storage_configuration = default_configuration()
        self.log = Logger
        self.common_storage_account = storage_configuration['common_storage_account']
        self.storage_account_prefix = storage_configuration['storage_account_prefix']
        self.resource_group_name = storage_configuration['resource_group_name']
        self.storage_client = Client(
            storage_configuration['credentials'],
            storage_configuration['subscription_id'])

    def get_account_keys(self, storage_account_name):
        """Return the keys for the given account name.

Returns a list of keys as a string. Returns an empty list if the account
cannot be found.

        """
        start = datetime.now().timestamp() * 1000
        try:
            result = self.storage_client.storage_accounts.list_keys(
                self.resource_group_name,
                storage_account_name
            )
            latency = round((datetime.now().timestamp() * 1000) - start)
            self.log.event({
                'state': 'ok',
                'service': 'detector retrieve account keys list latency',
                'metric': latency,
                'tags': ['storageaccount', 'account_keys', 'azure', 'retrieved', 'latency']
            })
            return [v.value for v in result.keys]
        except Exception as e:
            latency = round((datetime.now().timestamp() * 1000) - start)
            self.log.event({
                'state': 'critical',
                'service': 'detector retrieve account keys list latency',
                'metric': latency,
                'tags': ['storageaccount', 'account_keys', 'azure', 'failed', 'latency']
            })
            raise e
