# -*- coding: utf-8 -*-
"""
Top level __init__
"""

from __future__ import absolute_import
from pkg_resources import get_distribution, DistributionNotFound

try:
    # Change here if project is renamed and does not equal the package name
    # pylint: disable=C0103
    dist_name = 'camelot-detector'
    __version__ = get_distribution(dist_name).version

except DistributionNotFound:
    __version__ = 'unknown'
finally:
    del get_distribution, DistributionNotFound
