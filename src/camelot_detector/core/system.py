"""The Camelot Detector System"""

from camelot_detector.component import default_system
from camelot_detector.shared.system import add_component

def init_system(app):
    for key, value in default_system(app).items():
        add_component(key, value)
