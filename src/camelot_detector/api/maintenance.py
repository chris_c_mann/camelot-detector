"""Handling for detector maintenance jobs"""

from camelot_detector.shared.system import get_component
from os import environ

def clean_tasks(*args, **kwargs):
    """Clean tasks"""
    body = kwargs.get('body')
    if body and 'token' in body and environ['CAMELOT_DETECTOR_MAINTENANCE_TOKEN'] == body['token']:
        get_component('clean_up_scheduler').go()
        return None, 202
    else:
        return None, 403

def publish_metrics(*args, **kwargs):
    """Publish task metrics"""
    body = kwargs.get('body')
    if body and 'token' in body and environ['CAMELOT_DETECTOR_MAINTENANCE_TOKEN'] == body['token']:
        get_component('task').publish_metrics()
        return None, 204
    else:
        return None, 403
