"""Handling for learning"""

from camelot_detector.shared.system import get_component
from camelot_detector.shared.http_utils import from_bson

def learn_bulk(*args, **kwargs):
    """Learn from the results"""
    account_id = kwargs['user']
    account = get_component('account').get_account(account_id)
    if not account:
        # This would be bad, as we have made it past authn
        return None, 401
    if 'results' not in kwargs['body']:
        return 'Missing results', 400

    results = kwargs['body']['results']
    get_component('learn').learn_bulk(account, results)
    return "", 204
