"""Handlers for the account API"""

import re
import secrets
from flask_cors import cross_origin
from connexion.problem import problem
from camelot_detector.shared.system import get_component

def validate_registration(body):
    if 'email_address' not in body:
        return problem(400, 'Bad request', 'email_address expected but was not provided'), None

    email_address = body['email_address']
    if not re.search('^.+@(.+)\.(.+)+$', email_address):
        return problem(400, 'Bad request', 'Email address provided is not valid'), None

    return None, email_address

@cross_origin()
def register(*args, **kwargs):
    """Register a new account"""

    problem, email_address = validate_registration(kwargs['body'])
    if problem:
        return problem

    registered_account = get_component('account').register(email_address)

    if not registered_account:
        # Do not reveal who has an account
        return "", 204

    return "", 204

def auth(*args, **kwargs):
    account_id = kwargs['user']
    account = get_component('account').get_account(account_id)
    if not account:
        get_component('log').event({
            'state': 'critical',
            'description': 'account not found but authenticated',
            'tags': ['account', 'authentication', 'not_found']
        })
        # This would be bad, as we have made it past authn
        return None, 401
    get_component('log').event({
        'state': 'ok',
        'description': 'account authenticated',
        'tags': ['account', 'authentication', 'authenticated']
    })
    return None, 204

def delete(*args):
    """Delete an account"""
    # TODO Authenticate user
    # TODO Remove all containers
    # TODO remove all tasks
    # TODO remove all storage accounts
    # TODO remove account
    print(args)
