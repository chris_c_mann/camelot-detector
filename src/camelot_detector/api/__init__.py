import connexion
from camelot_detector.shared.system import is_worker

if is_worker():
    APP = None
else:
    APP = connexion.App(__name__, specification_dir='openapi/')
    APP.add_api('openapi.yml')
