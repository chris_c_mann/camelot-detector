"""API Authentication"""

from camelot_detector.shared.system import get_component

def basic_auth(email_address, password, required_scopes=None):
    account = get_component('account').authenticate(email_address, password)
    if not account:
        return None
    return {'sub': account['_id']}
