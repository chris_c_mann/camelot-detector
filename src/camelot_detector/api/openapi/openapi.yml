openapi: 3.0.0
info:
  title: Camelot Detector
  description: Image detection service
  termsOfService: https://camelotproject.org/terms/
  contact:
    name: Camelot Forums
    url: "https://groups.google.com/forum/#!forum/camelot-project"
  license:
    name: Apache 2.0
    url: https://www.apache.org/licenses/LICENSE-2.0.html
  version: 1.0.0
servers:
  - url: https://api.prod.camelotproject.org
    description: Production
  - url: https://api.stg.camelotproject.org
    description: Staging
  - url: http://localhost:8080
    description: Local
security:
  - BasicAuth: []
paths:
  /healthcheck:
    get:
      tags:
        - control
      security: []
      operationId: camelot_detector.api.control.healthcheck
      summary: Service health check
      responses:
        '200':
          description: OK
          content:
            text/plain:
              schema:
                type: string
  /account:
    post:
      tags:
        - account
      operationId: camelot_detector.api.account.register
      security: []
      summary: Register a new account. An app key will be issued via email to the given email address.
      parameters:
        - name: "email_address"
          in: body
          required: true
          schema:
            type: string
      responses:
        '204':
          description: Account registration received
    delete:
      tags:
        - account
      operationId: camelot_detector.api.account.delete
      summary: Delete the authenticated account and all associated data
      responses:
        '204':
          description: Account deletion successful.
        '401':
          description: Unauthorized
  /account/auth:
    post:
      tags:
        - account
      operationId: camelot_detector.api.account.auth
      summary: Attempt auth and return result
      parameters:
        - in: header
          name: x-camelot-version
          schema:
            type: string
          required: false
      responses:
        '204':
          description: Authentication successful
        '401':
          description: Unauthorized
  /maintenance/task/clean:
    post:
      tags:
        - maintenance
      security: []
      operationId: camelot_detector.api.maintenance.clean_tasks
      summary: Clean tasks
      parameters:
        - in: header
          name: x-camelot-version
          schema:
            type: string
          required: false
      requestBody:
        description: task maintenance
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/TaskMaintenance'
      responses:
        '202':
          description: Tasks scheduled for clean up
        '401':
          description: Unauthorized
  /maintenance/task/metrics:
    post:
      tags:
        - maintenance
      security: []
      operationId: camelot_detector.api.maintenance.publish_metrics
      summary: Publish task metrics
      parameters:
        - in: header
          name: x-camelot-version
          schema:
            type: string
          required: false
      requestBody:
        description: task maintenance
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/TaskMaintenance'
      responses:
        '204':
          description: Successfully published metrics
        '401':
          description: Unauthorized
  /task:
    post:
      tags:
        - task
      operationId: camelot_detector.api.task.task_create
      summary: Create a new task
      parameters:
        - in: header
          name: x-camelot-version
          schema:
            type: string
          required: false
      responses:
        '201':
          description: New task created successfully
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Task"
          links:
            GetTaskById:
              operationId: camelot_detector.api.task.task_get
              parameters:
                id: '$response.body#/1'
              description: >
                The `id` value returned in the response can be used as
                the `id` parameter in the `GET /task/{id} endpoint`.
            SubmitTaskById:
              operationId: camelot_detector.api.task.task_submit
              parameters:
                id: '$response.body#/1'
              description: >
                The `id` value returned in the response can be used as
                the `id` parameter in `GET /task/{id}/submit endpoint`.
            ArchiveTaskById:
              operationId: camelot_detector.api.task.task_archive
              parameters:
                id: '$response.body#/1'
              description: >
                The `id` value returned in the response can be used as
                the `id` parameter in `GET /task/{id}/archive endpoint`.
        '401':
          description: Unauthorized
  /task/{id}:
    get:
      tags:
        - task
      operationId: camelot_detector.api.task.task_get
      summary: Get the task with the given ID
      parameters:
        - in: header
          name: x-camelot-version
          schema:
            type: string
          required: false
        - name: id
          in: path
          description: Task ID
          required: true
          schema:
            type: string
      responses:
        '200':
          description: The given task was retrieved successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Task'
        '401':
          description: Unauthorized
        '404':
          description: Not found
  /task/{id}/submit:
    post:
      tags:
        - task
      operationId: camelot_detector.api.task.task_submit
      summary: Submit the task to the detector
      parameters:
        - in: header
          name: x-camelot-version
          schema:
            type: string
          required: false
        - name: id
          in: path
          description: Task ID
          schema:
            type: string
      responses:
        '204':
          description: The task has been submitted
        '401':
          description: Unauthorized
        '404':
          description: Not found
  /task/{id}/archive:
    post:
      tags:
        - task
      operationId: camelot_detector.api.task.task_archive
      summary: Archive the task removing the associated container
      parameters:
        - in: header
          name: x-camelot-version
          schema:
            type: string
          required: false
        - name: id
          in: path
          description: Task ID
          schema:
            type: string
      responses:
        '204':
          description: The task has been archived
        '401':
          description: Unauthorized
        '404':
          description: Not found
  /learn/bulk:
    post:
      tags:
        - task
      operationId: camelot_detector.api.learn.learn_bulk
      summary: Record learning data
      parameters:
        - in: header
          name: x-camelot-version
          schema:
            type: string
          required: false
      requestBody:
        description: bulk learning set
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/LearningSet'
      responses:
        '204':
          description: Learning data received successfully
        '401':
          description: Unauthorized
        '404':
          description: Not found

components:
  securitySchemes:
    BasicAuth:
      type: http
      scheme: basic
      x-basicInfoFunc: camelot_detector.api.auth.basic_auth

  schemas:
    Detection:
      type: object
      properties:
        category:
          type: string
          enum:
            - animal
            - person
            - vehicle
        confidence:
          type: number
        bounding_box:
          type: array
          items:
            type: number
    Image:
      type: object
      properties:
        file:
          type: string
        detections:
          type: array
          items:
            $ref: '#/components/schemas/Detection'
    Images:
      type: array
      items:
        $ref: '#/components/schemas/Image'
    TaskStatus:
      type: string
      enum:
        - PENDING
        - EXPIRED
        - SUBMITTED
        - COMPLETED
        - FAILED
    Container:
      type: object
      required:
        - name
        - readonly_sas
        - readwrite_sas
      properties:
        name:
          type: string
        readonly_sas:
          type: string
        readwrite_sas:
          type: string
    TaskMaintenance:
      type: object
      required:
        - token
      properties:
        token:
          type: string
    Task:
      type: object
      required:
        - id
        - container
        - container_expiry
        - status
      properties:
        id:
          type: string
        container:
          $ref: '#/components/schemas/Container'
        container_expiry:
          type: integer
        status:
          $ref: '#/components/schemas/TaskStatus'
        message:
          type: object
    LearningRecord:
      type: object
      required:
        - has_animals_actual
        - has_animals_detection_max_confidence
      properties:
        is_labelled:
          type: boolean
        has_animals_actual:
          type: boolean
        has_animals_detection_max_confidence:
          type: number
    LearningSet:
      type: object
      required:
        - results
      properties:
        results:
          type: array
          items:
            $ref: '#/components/schemas/LearningRecord'
