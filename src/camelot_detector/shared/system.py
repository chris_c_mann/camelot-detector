"""Shared system state"""

import sys
from functools import lru_cache

this = sys.modules[__name__]
this.system = {}

@lru_cache(maxsize=None)
def is_worker():
    return '--worker' in sys.argv[1:]

def add_component(name, value):
    """Add a component to the system definition."""
    this.system[name] = value

def get_component(name):
    """Return a component by name."""
    return this.system[name]
