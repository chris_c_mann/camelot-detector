"""HTTP utils."""

from failsafe import Failsafe, CircuitBreaker, RetryPolicy, FailsafeError
from camelot_detector.shared.system import get_component
import requests
import asyncio
import bson
import nest_asyncio

def _canonicalise_key(k):
    if k == '_id':
        return 'id'
    else:
        return k

def from_bson(o):
    if isinstance(o, dict):
        return { _canonicalise_key(key): from_bson(o[key]) for key in o.keys() }
    elif isinstance(o, list):
        return [ from_bson(v) for v in o ]
    elif isinstance(o, bson.ObjectId):
        return str(o)
    else:
        return o

retry_policy = RetryPolicy(allowed_retries=0)
circuit_breaker = CircuitBreaker(maximum_failures=3, reset_timeout_seconds=60)
failsafe = Failsafe(circuit_breaker=circuit_breaker, retry_policy=retry_policy)

def make_request(cb):
    initial_circuit_breaker_state = circuit_breaker.current_state
    try:
        return cb()
    finally:
        if initial_circuit_breaker_state != circuit_breaker.current_state:
            if circuit_breaker.current_state == 'open':
                get_component('log').event({
                    'state': 'warning',
                    'service': "detector batch_processor circuitbreaker opened count",
                    'metric': 1,
                    'tags': ['detector', 'http', 'circuitbreaker', 'open', 'count']
                })
            else:
                get_component('log').event({
                    'state': 'ok',
                    'service': "detector batch_processor circuitbreaker closed count",
                    'metric': 1,
                    'tags': ['detector', 'http', 'circuitbreaker', 'closed', 'count']
                })

def make_get_request_internal(url):
    async def _make_get_request():
        resp = requests.get(url, timeout=20.0)
        get_component('log').event({
            'state': 'ok',
            'service': "detector batch_processor GET response length",
            'metric': len(resp.text),
            'tags': ['detector', 'http', 'response', 'length']
        })
        if resp.status_code is not None and (resp.status_code < 200 or resp.status_code >= 300):
            get_component('log').event({
                'state': 'warning',
                'service': "detector batch_processor GET response statuscode %d count" % (resp.status_code),
                'metric': 1,
                'tags': ['detector', 'http', 'statuscode', 'response', 'get', 'count']
            })
            raise Exception()
        return resp.json()

    get_component('log').event({
        'state': 'ok',
        'service': "detector batch_processor GET request count",
        'metric': 1,
        'tags': ['detector', 'http', 'request', 'get', 'count']
    })
    try:
        nest_asyncio.apply()
        loop = asyncio.get_event_loop()
        return loop.run_until_complete(failsafe.run(_make_get_request))
    except FailsafeError:
        raise RuntimeError("Error while getting data")

def make_get_request(url):
    return make_request(lambda: make_get_request_internal(url))

def make_post_request_internal(url, data):
    async def _make_post_request():
        resp = requests.post(url, json=data, timeout=20.0)
        get_component('log').event({
            'state': 'ok',
            'service': "detector batch_processor POST response length",
            'metric': len(resp.text),
            'tags': ['detector', 'http', 'response', 'length']
        })
        if resp.status_code is not None and (resp.status_code < 200 or resp.status_code >= 300):
            get_component('log').event({
                'state': 'warning',
                'service': "detector batch_processor POST response statuscode %d count" % (resp.status_code),
                'metric': 1,
                'tags': ['detector', 'http', 'statuscode', 'response', 'post', 'count']
            })
            raise Exception()
        return resp.json()

    get_component('log').event({
        'state': 'ok',
        'service': "detector batch_processor POST request count",
        'metric': 1,
        'tags': ['detector', 'http', 'request', 'post', 'count']
    })
    try:
        nest_asyncio.apply()
        loop = asyncio.get_event_loop()
        return loop.run_until_complete(failsafe.run(_make_post_request))
    except FailsafeError:
        raise RuntimeError("Error while getting data")

def make_post_request(url, data):
    return make_request(lambda: make_post_request_internal(url, data))
